# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. Chatroom
    2. Forum
* Other functions (add/delete)
    1. 使用者在account的mypage中可以修改自己的個人資料
    2. 使用者可以通過Facebook登入
    3. 使用者修改密碼等安全舉動時會發送郵件到郵箱
    4. 輸入“!play”會有音樂XD，特別經典老歌啦（再輸入“!stop”取消）

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|
由於Gitlab較不穩定，因此我使用的是firebase，網頁可在firebase提供的平台上運行。database與RWD都已完成，Membership Mechanism可使用Google，Facebook，
以及郵箱登錄，且可自創賬號，在輸入有誤時會產生相應的錯誤資訊，讓使用者能及時更改。
登錄進去之後，頁面上會顯示一串list，這些list是目前所有的頻道，然後在“Chat at”一欄中輸入想前往的頻道，再點擊下方的按鈕變回進入。在進入一個頻道后，會看到目前頻道內的消息，消息上附有發言人，下方則是消息內容。當發言人姓名無法顯示時，可以通過顯示郵箱作為替代。通過在“New post”欄中輸入想發送的信息再按按鈕會發送出去。頻道間可任意切換，而若欲回到開始時的頻道欄
只需要點擊左上角“Shui Forum”即可
Account點開后會出現幾個選項，第一個為使用者登記的郵箱，下一個為“My page”，點擊可進入使用者資料頁面，資料頁面會顯示當前使用者的姓名，郵箱，以及UID，同時左上方會有“homepage”，點擊可以回到起始頁。
點開“Chatroom”后則會進入Chatroom功能。Chatroom每发送一条消息银幕会跟着滚动，会显示发消息的时间以及发信息人的姓名。
“Shopping”一欄下有“product”與“dashboard”兩個選項，由於尚未有好的想法，因此只好使用iframe鑲嵌別的頁面了QQ。
此外，每個頁面account中都有logout，可隨時登出。
## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|
第三方登入有Google與Facebook。
在頻道內當發送或收到消息時會收到瀏覽器發來的notification，且會顯示發送者的姓名。
當進入account中的分頁時，頁面中間紫色部分會發生緩慢變色，且此功能可兼容各大瀏覽器。
## Website Detail Description
詳細訊息打在每一個components下面
## Security Report (Optional)
在登入進頁面輸入密碼時為不可視狀態，可以有效保護密碼避免洩漏。在發送及顯示信息中，除了可以設置的姓名以外沒有過多信息透露，有效保護使用者隱私。且密碼可以變更，在使用者密碼被他人得知后可以迅速完成修改。而在修改郵箱或密碼時，使用者新郵箱或現在郵箱會收到郵件，以此來保證使用者修改是自己的操作。