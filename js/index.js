function init() {
    var user_email = '';
    var user_displayName = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu1 = document.getElementById('dynamic-menu1');
        var menu2 = document.getElementById('dynamic-menu2');
        if (user) {
            user_email = user.email;
            user_displayName = user.displayName;
            menu1.innerHTML = "<span class='dropdown-item'>" + user.email 
            + "</span><a href='Mypage.html' class='dropdown-item' id='mypage' >My page</a>"
            + "<span><a href='Userpage.html' class='dropdown-item'>Chatroom</a>" +
            "</span>" +"<span class='dropdown-item' id='logout-btn'>Logout</span>";
            menu2.innerHTML = "</span><a class='dropdown-item' href='Shoppingpage.html'>Product</a>" + 
            "</span><span class='dropdown-item' id='dash-btn'>Dashboard</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu1.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            menu2.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    /*post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
    });*/

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent channels</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref("/");
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    document.getElementById('Home').addEventListener('click', function () {
        postsRef = firebase.database().ref("/");
        var total_post = [];
        var first_count = 0;
        var second_count = 0;
        postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {       
                total_post[total_post.length] = str_before_username + childSnapshot.key + "</strong>" + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    total_post[total_post.length] = str_before_username + childSnapshot.key + "</strong>" + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
    });
    if(postsRef.toString() == firebase.database().ref("/").toString()){
        postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {         
                total_post[total_post.length] = str_before_username + childSnapshot.key + "</strong>" + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    total_post[total_post.length] = str_before_username + childSnapshot.key + "</strong>" + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
    }
    chat_name = document.getElementById('chatname');
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    go_btn = document.getElementById('gothere_btn');
    go_btn.addEventListener('click', function () {
        if (chat_name.value != "") {
            var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent posts</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                if(chat_name.value[0]<'0'||chat_name.value[0]>'9') {
                post_btn.addEventListener('click', function() {
                    if (post_txt.value != "") {
                        var newpostref = firebase.database().ref(chat_name.value).push();
                        /*alert(newpostref.key);
                        var newspost = firebase.database().ref(chat_name.value+"/"+newpostref.key.toString()).push();*/
                        newpostref.set({
                            email: user_email,
                            data: post_txt.value,
                            name: user_displayName
                        });
                    post_txt.value = "";
                    }
                });
            var str_after_content = "</p></div></div>\n";
            var postsRef = firebase.database().ref(chat_name.value);
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        if (childData.name == '') childData.name = childData.email;
                        total_post[total_post.length] = str_before_username + childData.name + "</strong>" + childData.data + str_after_content;
                        first_count += 1;
                    });
                    document.getElementById('post_list').innerHTML = total_post.join('');
        
                    //add listener
                    postsRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            if (childData.name == '') childData.name = childData.email;
                            total_post[total_post.length] = str_before_username + childData.name + "</strong>" + childData.data + str_after_content;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                            if(childData.data=="!play"){document.getElementById("playmusic").innerHTML = "<audio controls loop autoplay><source src='distant suns.mp3' type='audio/mpeg'></audio>";}
                            else if(childData.data=="!stop"){document.getElementById("playmusic").innerHTML = "";}
                            if (window.Notification) {
                                
                                var popNotice = function() {
                                    if (Notification.permission == "granted") {
                                        var notification = new Notification("Hi，朋友：", {
                                            body: childData.name + ' send a message',
                                            icon: 'http://image.zhangxinxu.com/image/study/s/s128/mm1.jpg'
                                        });
                                        
                                        notification.onclick = function() {
                                            notification.close();    
                                        };
                                    }    
                                };
                                
                                    if (Notification.permission == "granted") {
                                        popNotice();
                                    } else if (Notification.permission != "denied") {
                                        Notification.requestPermission(function (permission) {
                                          popNotice();
                                        });
                                    }
                            } else {
                                alert('浏览器不支持Notification');    
                            }
                        }
                    });
                })
                .catch(e => console.log(e.message));
            }
            else{
                alert("Post name can't be numbers!");
                chat_name.value = "";
            }
        }
    });
}

window.onload = function () {
    init();
}