function init() {
    var user_email = '';
    var user_name = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu1 = document.getElementById('dynamic-menu1');
        var menu2 = document.getElementById('dynamic-menu2');
        if (user) {
            user_email = user.email;
            user_name = user.displayName;
            menu1.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><a href='Mypage.html' class='dropdown-item'>My page</a>" +
            "<a class='dropdown-item' id='logout-btn' href='index.html' >Logout</a>";
            menu2.innerHTML = "</span><span class='dropdown-item' id='product-btn'>Product</span>" + 
            "</span><span class='dropdown-item' id='dash-btn'>Dashboard</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu1.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            menu2.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    // Get elements
    var chat_btn = document.getElementById('chat_btn');
    var chat_txt = document.getElementById('chat_txt');

    // Store to database when post comments
    chat_btn.addEventListener('click', function () {

        if (chat_txt.value != "") {
            var newpostref = firebase.database().ref('chat_list').push();
            var currentTime = Date.now();
            newpostref.set({
                name: user_name,
                email: user_email,
                data: chat_txt.value,
                time: currentTime
            });
            chat_txt.value = "";
        }
    });

    // Show content when database update
    var str_before_username ="<div class='media text-muted pt-3'>" + 
    "<img src='chataccount.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>" + 
    "<p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'>";
    var str_after_content = "</p></div>\n";

    var chatsRef = firebase.database().ref('chat_list');
    var total_chat = [];
    var first_count = 0;
    var second_count = 0;

    chatsRef.once('value')      // show chats that exist for once
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_chat[total_chat.length] = new Date(childData.time) + str_before_username + "<b>"
                    + childData.name + "</b>" + ": " +"<span class='mb-0 lh-100'>" + childData.data + "</span>"
                    + str_after_content;
                first_count += 1;
            });
            document.getElementById('chatroom').innerHTML = total_chat.join('');
            var div = document.getElementById("chattroom");
            div.scrollTop = div.scrollHeight - div.clientHeight;

            chatsRef.on('child_added', function (data) { 
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_chat[total_chat.length] = new Date(childData.time) + str_before_username + "<b>"
                        + childData.name + "</b>"+ ": " + "<span class='mb-0 lh-100'>" + childData.data + "</span>"
                        + str_after_content;
                    document.getElementById('chatroom').innerHTML = total_chat.join('');
                    var div = document.getElementById("chattroom");
                    div.scrollTop = div.scrollHeight - div.clientHeight;
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}