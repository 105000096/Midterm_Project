function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu1 = document.getElementById('dynamic-menu1');
        var menu2 = document.getElementById('dynamic-menu2');
        var user_email = user.email;
        if (user) {
            var i = 0;
            menu1.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><a href='Userpage.html' class='dropdown-item'>Chat room</a>" +
            "<a class='dropdown-item' id='logout-btn' href='index.html' >Logout</a>";
            menu2.innerHTML = "</span><span class='dropdown-item' id='product-btn'>Product</span>" + 
            "</span><span class='dropdown-item' id='dash-btn'>Dashboard</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            cname_btn = document.getElementById('cname_btn');
            cname_txt = document.getElementById('cname');
            cemail_btn = document.getElementById('cemail_btn');
            cemail_txt = document.getElementById('cemail');
            cpassword_btn = document.getElementById('cpassword_btn');
            cpassword_txt = document.getElementById('cpassword');
            var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>My account</h6><div class='media text-muted pt-3'><img src='account.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 big lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            cname_btn.addEventListener('click', function() {
                if (cname_txt.value != ""){
                    var user = firebase.auth().currentUser;
                    user.updateProfile({displayName:cname_txt.value}).then(function(){
                        alert("Success~");
                        var total_post = [];
                        var first_count = 0;
                        var second_count = 0;
                        total_post[0] = str_before_username + "My name:  " + user.displayName + 
                        "</strong><br>" + "<strong class='d-block text-gray-dark'>My email:  " + user.email + 
                        "</strong><br>" + 
                        "<strong class='d-block text-gray-dark'>My uid:  " + user.uid + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }).catch(function(error){
                        alert("Error!");
                    });
                    var str_after_content = "</p></div></div>\n";
                /*var postsRef = firebase.database().ref("/");*/
                }
                cname_txt.value = "";
            });
            cemail_btn.addEventListener('click', function() {
                if (cemail_txt.value != ""){
                    var user = firebase.auth().currentUser;
                    user.updateEmail(cemail_txt.value).then(function(){
                        alert("Success~");
                        var total_post = [];
                        var first_count = 0;
                        var second_count = 0;
                        total_post[0] = str_before_username + "My name:  " + user.displayName + 
                        "</strong><br>" + "<strong class='d-block text-gray-dark'>My email:  " + user.email + 
                        "</strong><br>" + 
                        "<strong class='d-block text-gray-dark'>My uid:  " + user.uid + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }).catch(function(error){
                        alert("Error!");
                    });
                    user.sendEmailVerification().then(function() {
                        alert("Send email succeed!");
                    }).catch(function(error) {
                        alert("Error!");
                    });
                }
                cemail_txt.value = "";
            });
            cpassword_btn.addEventListener('click', function() {
                if (cpassword_txt.value != ""){
                    var user = firebase.auth().currentUser;
                    user.updatePassword(cpassword_txt.value).then(function(){
                        alert("Success~");
                        var total_post = [];
                        var first_count = 0;
                        var second_count = 0;
                        total_post[0] = str_before_username + "My name:  " + user.displayName + 
                        "</strong><br>" + "<strong class='d-block text-gray-dark'>My email:  " + user.email + 
                        "</strong><br>" + 
                        "<strong class='d-block text-gray-dark'>My uid:  " + user.uid + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }).catch(function(error){
                        alert("Error!");
                    });
                    user.sendEmailVerification().then(function() {
                        alert("Send email succeed!");
                    }).catch(function(error) {
                        alert("Error!");
                    });
                }
                cpassword_txt.value = "";
            });
               /* post_btn.addEventListener('click', function() {
                    if (password_txt.value != "") {
                        alert('right');
                        var newpostref = firebase.database().ref('Chat_room/').ref('room1').push();
                        alert('right');
                        newpostref.set({
                            email: user_email,
                            data: post_txt.value
                        });
                    password_txt.value = "";
                    post_txt.value = "";
                    }
                });*/
            var str_after_content = "</p></div></div>\n";
            /*var postsRef = firebase.database().ref("/");*/
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            total_post[0] = str_before_username + "My name:  " + user.displayName + 
            "</strong><br>" + "<strong class='d-block text-gray-dark'>My email:  " + user.email + 
            "</strong><br>" + 
            "<strong class='d-block text-gray-dark'>My uid:  " + user.uid + str_after_content;
            document.getElementById('post_list').innerHTML = total_post.join('');
            /*postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                        first_count += 1;
                    });
                    document.getElementById('post_list').innerHTML = total_post.join('');
        
                    //add listener
                    postsRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                        }
                    });
                })
                .catch(e => console.log(e.message));*/
            
        } else {
            menu1.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            menu2.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
}

window.onload = function () {
    init();
}